#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#======================================================================================#
#                                   WSDM                                                #
#======================================================================================#
"""WSDM_tools: Wetland Soils Denitrification Model tools script."""
__credits__ = "C. Martinez-Espinosa, S. Sauvage, J. M. Sanchez-Perez, A. Al Bitar"
__authors__ = "A. Al Bitar & C. Martinez-Espinosa"
__copyright__ = "ECOLAB-CESBIO-OMP"
__license__ = "GPL"
__version__ = "0.1"
#======================================================================================#

## import modules and libraires
import os
import glob
from datetime import datetime
import numpy as np
from scipy.io import netcdf
import scipy
import time
import rasterio
#import xarray as xr

import WSDM_io
import WSDM_plots



def resample_soil(Bulk,OrgC,CLPC,CNrt,TCEQ,dx):
    Bulk_int=Bulk[::dx,::dx]
    OrgC_int=OrgC[::dx,::dx]
    CLPC_int=CLPC[::dx,::dx]
    CNrt_int=CNrt[::dx,::dx]
    TCEQ_int=TCEQ[::dx,::dx]
    
    Bulk_int=np.where(Bulk_int<0.0,np.nan,Bulk_int)
    OrgC_int=np.where(OrgC_int<0.0,np.nan,OrgC_int)
    CLPC_int =np.where(CLPC_int<0.0,np.nan,CLPC_int)
    CNrt_int=np.where(CNrt_int<0.0,np.nan,CNrt_int)
    TCEQ_int=np.where(TCEQ_int<0.0,np.nan,TCEQ_int)
    
    return Bulk_int,OrgC_int,CLPC_int,CNrt_int,TCEQ_int

def generate_KOC(land_class,params):
    land_class_int = land_class[::params['res_reduc'][0],::params['res_reduc'][0]]
    KOC_opt_int    = np.zeros(np.shape(land_class_int))+params['K_OC_grassland'][0] 
   
    KOC_opt_int=np.where(land_class_int==12 ,params['K_OC_25_wetlands'][0],KOC_opt_int)   
    KOC_opt_int=np.where(land_class_int==11 ,params['K_OC_irrigated_croplands'][0],KOC_opt_int)
    KOC_opt_int=np.where(land_class_int==14 ,params['K_OC_crop_rainfed'][0],KOC_opt_int)
    
    KOC_opt_int=np.where(land_class_int==20 ,params['K_OC_cropland_mosaic'][0],KOC_opt_int)
    
    KOC_opt_int=np.where(land_class_int==5  ,params['K_OC_flooded_forest'][0],KOC_opt_int)
    KOC_opt_int=np.where(land_class_int==160,params['K_OC_flooded_forest'][0],KOC_opt_int)
    
    KOC_opt_int=np.where(land_class_int==30 ,params['K_OC_shrubland'][0],KOC_opt_int)
    KOC_opt_int=np.where(land_class_int==110,params['K_OC_shrubland'][0],KOC_opt_int)
    KOC_opt_int=np.where(land_class_int==150,params['K_OC_shrubland'][0],KOC_opt_int)

    KOC_opt_int=np.where(land_class_int==4  ,params['K_OC_floodplain'][0],KOC_opt_int)
    KOC_opt_int=np.where(land_class_int==9  ,params['K_OC_floodplain'][0],KOC_opt_int)
    KOC_opt_int=np.where(land_class_int==10 ,params['K_OC_floodplain'][0],KOC_opt_int)
    KOC_opt_int=np.where(land_class_int==180,params['K_OC_floodplain'][0],KOC_opt_int)
    
    KOC_opt_int=np.where(land_class_int==8  ,params['K_OC_peatland'][0],KOC_opt_int)

    KOC_opt_int=np.where(land_class_int==6  ,params['K_OC_brackish'][0],KOC_opt_int)
    KOC_opt_int=np.where(land_class_int==7  ,params['K_OC_brackish'][0],KOC_opt_int)    
    KOC_opt_int=np.where(land_class_int==170,params['K_OC_brackish'][0],KOC_opt_int)

    KOC_opt_int=np.where(land_class_int==40 ,params['K_OC_forest'][0],KOC_opt_int)
    KOC_opt_int=np.where(land_class_int==50 ,params['K_OC_forest'][0],KOC_opt_int)
    KOC_opt_int=np.where(land_class_int==60 ,params['K_OC_forest'][0],KOC_opt_int)
    KOC_opt_int=np.where(land_class_int==70 ,params['K_OC_forest'][0],KOC_opt_int)
    KOC_opt_int=np.where(land_class_int==90 ,params['K_OC_forest'][0],KOC_opt_int)
    KOC_opt_int=np.where(land_class_int==100,params['K_OC_forest'][0],KOC_opt_int)                

    KOC_opt_int=np.where(land_class_int==120,params['K_OC_grassland'][0],KOC_opt_int)    
    KOC_opt_int=np.where(land_class_int==130,params['K_OC_grassland'][0],KOC_opt_int)    
    KOC_opt_int=np.where(land_class_int==140,params['K_OC_grassland'][0],KOC_opt_int) 
          
    KOC_opt_int=np.where(land_class_int==1,params['K_OC_water'][0],KOC_opt_int)    
    KOC_opt_int=np.where(land_class_int==2,params['K_OC_water'][0],KOC_opt_int)    
    KOC_opt_int=np.where(land_class_int==3,params['K_OC_water'][0],KOC_opt_int) 
    KOC_opt_int=np.where(land_class_int==210,params['K_OC_water'][0],KOC_opt_int) 

    
    
    return KOC_opt_int,land_class_int


################# resample all the other databases (SM, TEMP, LANDUSE)

def log_info_message(info_message,message):
    message="[info]" + str(datetime.now())+' '+message
    info_message.append(message)
    print(message)
    return info_message

def remap(values,index_list,shape):
    mapx= np.empty(shape) #, dtype='float32')
   # mapx[index_list[0],index_list[1]]=values.astype(np.float32)
    n=0
    #bg=values.astype(np.float32)
    for i in range(np.size(index_list[0])): 
        mapx[index_list[0][i]][index_list[1][i]]=values[n]
        n=n+1
        
    return mapx

def SMmax(smos_dir):
    SM_files_list=sorted(glob.glob(smos_dir+'/*/*.nc'))
    SM1max=np.zeros((584,1388))
    SM2max=np.zeros((584,1388))
    
    nc  = netcdf.netcdf_file(SM_files_list[0],'r')
    lon = nc.variables['lon'][:].copy()
    lat = nc.variables['lat'][:].copy()
    nc.close()   
        
    for i_file in range(len(SM_files_list)):
        print(SM_files_list[i_file])
        nc  = netcdf.netcdf_file(SM_files_list[i_file],'r')
        SM2 = nc.variables['SM2'][:,:].copy()
        nc.close()
     
        SM2max= np.maximum(SM2max,SM2)
    SM2max=np.flipud(SM2max)  
    df=.DataArray(SM2max,dims= dict(lon=(lon),lat=(lat)), attrs=dict(description="SM2MAX_lr.",units="m3/M3"))

    df.to_netcdf('../data/SMmax.nc')
    return lon,lat,SM1max,SM2max   

def SMmax_hr(data_dir):
    # get list of files: 


    SM_files_list=sorted(glob.glob(os.path.join(data_dir,'../SM/*/*201[1,2,3,4,5,6,7,8,9]*T*.nc')))
    #SM_files_list=sorted(glob.glob(smos_dir+'/*/*.nc'))    
    
    #1 read one file to get lat, lon of the EASE2 grid 25km SMOS
    nc  = netcdf.netcdf_file(SM_files_list[0],'r')
    lon_lr = nc.variables['lon'][:].copy()
    lat_lr = nc.variables['lat'][:].copy()
    nc.close() 
  
    
    #2 get the land sea mask for EASE2
    nc  = netcdf.netcdf_file(os.path.join(data_dir,'AUX_LANDCOVER/SM_TEST_AUX_LANDCO_20050101T000000_20500101T000000_007_001_5.DBL'),'r')
  
    ease_land_sea = nc.variables['Land_Sea_Mask'][:,:].copy()
    nc.close()


    #4 read lat, lon of the high resolution grid SMOS
    nc  = netcdf.netcdf_file(os.path.join(data_dir,'SOIL/NetCDF_files/BULK.nc'),'r')
    lon_hr = nc.variables['lon'][:].copy()
    lat_hr = nc.variables['lat'][:].copy()
    nc.close()
    
    #5 intitialize the global map SMmax
    SM2max_world=np.zeros((np.size(lat_hr,0),np.size(lon_hr,0)))
    
    # define regions    
    world_coord_cut_name=["zone1","Zone2","Zone3","zone4","zone5"]
    world_coord_cut_lon_min=[-170,-20, 39, 69,99]
    world_coord_cut_lon_max=[-32,  40, 70, 100,179]
    world_coord_cut_lat_min=[-60, -45,-45,-45,-45]
    world_coord_cut_lat_max=[75,  75 , 75, 75,75]
    
    # 6. run over regions
    for i_wise in range(len(world_coord_cut_name)):

         #6.1 determine world continent limit coordonates
        lonmin=world_coord_cut_lon_min[i_wise]
        lonmax=world_coord_cut_lon_max[i_wise]
        latmin=world_coord_cut_lat_min[i_wise]
        latmax=world_coord_cut_lat_max[i_wise]
        print("Reading",world_coord_cut_name[i_wise],"coordinates ")
         #6.2 extract the selected grid
         
        [idx_lonmin, idx_lonmax, idx_latmin, idx_latmax]=WSDM_io.get_ROI_coord_indexs(lon_hr, lat_hr, lonmin, lonmax, latmin, latmax)         
        lon_wise_hr = lon_hr[idx_lonmin:idx_lonmax]
        lat_wise_hr = lat_hr[idx_latmax:idx_latmin]
        
        [idx_lonmin_lr, idx_lonmax_lr, idx_latmin_lr, idx_latmax_lr]=WSDM_io.get_ROI_coord_indexs(lon_lr, lat_lr, lonmin-0.5, lonmax+0.5, latmin-0.5, latmax+0.5)
        lon_wise_lr = lon_lr[idx_lonmin_lr:idx_lonmax_lr]
        lat_wise_lr = lat_lr[idx_latmin_lr:idx_latmax_lr]

        # 6.3 generate meshgrid
        [lon_wise_hr_mesh,lat_wise_hr_mesh]=np.meshgrid(lon_wise_hr,lat_wise_hr)
        [lon_wise_lr_mesh,lat_wise_lr_mesh]=np.meshgrid(lon_wise_lr,lat_wise_lr)
        
        SM2max=np.zeros((np.size(lat_wise_hr,0),np.size(lon_wise_hr,0)))
               
        
        for i_file in range(len(SM_files_list)):
       # for i_file in range(0,0):
            print("proc file:",SM_files_list[i_file])
            nc  = netcdf.netcdf_file(SM_files_list[i_file],'r')
            SM2 = nc.variables['SM2'][idx_latmin_lr:idx_latmax_lr,idx_lonmin_lr:idx_lonmax_lr].copy()
            nc.close()
            
            t = time.time()
            SM2_hr=scipy.interpolate.griddata((lat_wise_lr_mesh.flatten(),lon_wise_lr_mesh.flatten()),SM2.flatten(),(lat_wise_hr_mesh,lon_wise_hr_mesh),method='linear')
            elapsed = time.time() - t    
            print(elapsed)
            SM2max= np.maximum(SM2max,SM2_hr)

        SM2max_world[idx_latmax:idx_latmin,idx_lonmin:idx_lonmax]=SM2max
        
    return lon_hr,lat_hr,SM2max_world
  

def sub2ind(array_shape, rows, cols):
    ind = rows*array_shape[1] + cols
    ind[ind < 0] = -1
    ind[ind >= array_shape[0]*array_shape[1]] = -1
    return ind

def ind2sub(array_shape, ind):
    ind[ind < 0] = -1
    ind[ind >= array_shape[0]*array_shape[1]] = -1
    rows = (ind.astype('int') / array_shape[1])
    cols = ind % array_shape[1]
    return (rows, cols)

def extract_somme_wetl_type(target_array,array_selection,type_wetl):
    temp=array_selection    
    temp[temp!=type_wetl]=0
    temp[temp==type_wetl]=1
    # np.unique(temp)

    target_array_select=target_array*temp
    
    sum_target_array_select=target_array_select.sum()
    
    return sum_target_array_select


def cut_mask(target_array,params):
    rasterD = rasterio.open(params['mask'][0])
    # bsn_msk=params['Watershed_name'][0]
    bsn_msk = rasterD.read(1)
# # Cut the region interest in the mask
    [LON_wise, LAT_wise] = WSDM_io.get_lon_lat_vectors('data/SOIL/NetCDF_files/BULK.nc')
    [idx_lonmin_wise, idx_lonmax_wise, idx_latmin_wise, idx_latmax_wise]= WSDM_io.get_ROI_coord_indexs(LON_wise, LAT_wise, params['lonmin'][0], params['lonmax'][0],params['latmin'][0], params['latmax'][0])

    bsn_msk_select=bsn_msk[idx_latmax_wise:idx_latmin_wise,idx_lonmin_wise: idx_lonmax_wise]
    bsn_msk_select = bsn_msk_select.astype("float")
    bsn_msk_select[bsn_msk_select==255]=np.NaN
    
    target_array_new=target_array*bsn_msk_select
    
    return target_array_new

 
if __name__ == "__main__":
      print("main function")
  
      data_dir='../../../database/data/'
      lon_hr,lat_hr,SM2max_world=SMmax_hr(data_dir)
      SM2max_nan= np.nan_to_num(SM2max_world)
      SM2max_nan.max() 

      np.save('./SM2hrmax_final',SM2max_world,allow_pickle=True)
      
      f=netcdf.netcdf_file('SM2max.nc', 'w')
      f.createDimension('lon', len(lon_hr))
      f.createDimension('lat', len(lat_hr))
      vlon=f.createVariable('Lon', 'f4', ('lon',))
      vlat=f.createVariable('Lat', 'f4', ('lat',))
      vSM2max=f.createVariable('SM2max', 'f4', ('lat','lon'))
      vSM2max=SM2max_world
      f.close()
      WSDM_plots.make_map(lat_hr,lon_hr,SM2max_nan,'SMmax.png','SMmax',0.0,0.6)


      

   
