#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#======================================================================================#
#                                   WSDM                                               #
#======================================================================================#
"""WSDM_Model : Wetland Soils Denitrification Model nitrification, denitrification and budget script."""
__credits__ = "C. Martinez-Espinosa, S. Sauvage, J. M. Sanchez-Perez, A. Al Bitar"
__authors__ = "A. Al Bitar & C. Martinez-Espinosa"
__copyright__ = "ECOLAB-CESBIO-OMP"
__license__ = "GPL"
__version__ = "0.1"
#======================================================================================#"""

import numpy as np
import os
import matplotlib.pyplot as plt
import pandas as pd
from scipy.io import netcdf


def nitrification(OrgC, C_N, bulk, TCEQ, CLPC,SM,SMmax,Temp, Th_SM_mina):
    """
    Compute nitrates content (µg/g) = (mgN/kg) (eq.2)
    input: 
        OrgC: Organic carbon g/kg
        C/N ratio
        Bulk: Bulk denisty
        TCEQ: Carbonate content gC/kg
        CLPL: Clay (%)
        SM: Soil moisture (m3 water m-3 soil)

    Parameters
    ----------
    bulk :  2D array
        Bulk density.
    OrgC : 2D array
       Organic Carbon content.
    CLPC : 2D array
        Clay percentage.
    C_N :2D array
      C:N ratio.
    TCEQ : 2D array
        Total carbonate equivalent.
    SM : 2D array
        soil moisture
    SMmax : 2D array
       Max soil moisture.
    Temp : 2D array
       Soil temperature
    Th_SM_mina : int
       Threshold of soil moisture for nitirification.

    Returns
    -------
    NO3 : 2D array
        nitrates in soil (mgN/kg) .
    """
  
    WP=SMmax/3
    SME = np.nan_to_num((np.nan_to_num(SM)-WP)/(SMmax-WP))
    k2 = ((1200)/((CLPC+200)*((0.3*TCEQ)+200)))*bulk*(0.2*(Temp-10))
    OrgN= OrgC/C_N 
    Nitrates= OrgN * SME * k2
    value=(Nitrates*1000)/365

    NO3_act= np.where(SME > Th_SM_mina,0.0,value) 
    NO3 = np.nan_to_num(np.where(NO3_act>0, NO3_act, 0.0))
    
    
    
    return NO3


def denit_rate(K_OC,BULK,ORG_C,NO3_act,K_NO3,redfr,SM,SMmax, Temp, Temp_opt,Th_SM_denit,depth1):
    """Compute denitrification Rate (eq. 3)
    K_OC: Rate of decomposition of available organic matter (d-1)
    ORG_C: organic carbon (g C/kgSoil)
    Bulk: dry sediment density (kg/ dm3)
    PORO: sediment porosity        (no unit))
    NO3_act: nitrate concentration (µg  N / g soil )
    K_NO3: half-saturation constant 
    SM: soil moisture (m3/m3)
    SMmax: maximum soil moisture (m3/m3)
    R_Denit_mol (denitrification rate) (mol_NO3/ dm3 soil /d)   
    output:
    R_Denit (denitrificaiton rate) (µg/g) =(mgN/kg) 

    Parameters
    ----------
    K_OC : float
       carbon decomposition rate depending wetlands typology. 
    BULK :  2D array
        Bulk density.
    ORG_C : 2D array
       Organic Carbon content.
    NO3_act :  2D array
        nitrates in soil.
    K_NO3 : float
       half saturatuin constant.
    redfr :float
     redfield coheficiente
    SM : 2D array
        soil moisture.
    SMmax : 2D array 
        max soil moisture
    Temp : 2D array
        soil temperature.
    Temp_opt : int
        optimal temperature.
    Th_SM_denit : float
        threshold of soil moisture for denitrification 
    depth1 : float
        soil depth

    Returns
    -------
    R_Denit_fin : 2D array
        denitrification rate.

    """   
   
    #1.  compute soil characteristics 
        
    PORO=(1-(BULK/2.65))
    SOIL =(BULK*(1-PORO)/PORO )
    
    # temperature factor
    T_fac= np.nan_to_num(np.exp((-(Temp - Temp_opt)**2)/(Temp* Temp_opt)))
    
    #2.  compute orgc_decomp (mol/d)(conversion to mol using Carbon molar mass)
    DECOM =( K_OC *(ORG_C/12))
    
    #3. compute Nitrate in soil limiting factor (unitless) input in (mgN/kg) 
  
    NO3_CONC=np.true_divide(NO3_act,NO3_act+K_NO3, where=((NO3_act+K_NO3)!=0))
    FN=np.nan_to_num(NO3_CONC)
  
    
    #3. compute soil moisture effect (unitless)
    WP=(SMmax/3)
   
    SME_1 = np.nan_to_num((np.nan_to_num(SM)-WP)/(SMmax-WP))
   
    SME= np.where (SME_1 < 0,0,SME_1)

    #4. agregate components
    R_Denit= np.nan_to_num(0.8 * redfr * SOIL * DECOM * FN * SME *  T_fac * 0.5)
    #5. Apply threasholds

    R_Denit_mole = np.nan_to_num(np.where(SME >= Th_SM_denit, R_Denit,0.0)) 

      #6. Convertion from mol/dm3 d-1 to (µg/g)=(mgN/kg) d-1
    R_Denit_fin = np.nan_to_num(R_Denit_mole/SOIL * 14 *1000) 

         
    return R_Denit_fin


def NO3_budget(NO3_t1,R_Denit,NO3_act,dt):
    """compute NO3 budget (eq.1)
    output: nitrates in soil (mgN/kg)d-1 
    
    Parameters
    ----------
    NO3_t1 : 2D array
        nitrates in soil.
    R_Denit : 2D array
        denitrification in soil.
    NO3_act : 2D array
       nitrates produced.
    dt : int
        time lap.

    Returns
    -------
    NO3_t2 : 2D array
        nitrates available in soil after denitritifacion.

    """
    
    NO3_t2=np.where(NO3_t1 + NO3_act* dt - R_Denit* dt  <0, 0.0, NO3_t1 -  R_Denit * dt + NO3_act * dt)
    return NO3_t2


def denit_update(R_Denit, NO3_t1, NO3_act):
    """adjust the denitrification according to the NO3 budget available 
    output: real dneitrification (mgN/kg)d-1
    

    Parameters
    ----------
    R_Denit : 2D array
        denitrification rate.
    NO3_t1 :2D array
        nitrates available in the soil .
    NO3_act :2D array
       nitrates production

    Returns
    -------
    R_Denit_Update : 2D array
        actual denitrification.

    """
   
    R_Denit_Update = np.where(NO3_t1 + NO3_act - R_Denit <0, NO3_t1 + NO3_act, R_Denit)
    
    return R_Denit_Update


#######################################################################
if __name__ == "__main__" :   
    
   
    # testing the functions:
    ## 1. denitrification:
    Temp   = 25  
    BULK   = 1.32 
    SM     = np.arange(0.01,0.9, 0.01)
    SMmax  = 1
    C_N    = 7
    ORG_C  = 10.0 
    depth1 = 0.3
    Temp_opt= 25
    redfr = 5
    Th_SM= 0.7
    K_OC = 0.32
    K_NO3 = 603
    TCEQ= 2 
    CLPC= 40

    NO3_act=  nitrification(ORG_C, C_N, BULK , TCEQ, CLPC,SM,SMmax,Temp, Th_SM)

    SM= 0.8
    denit= denit_rate(K_OC,BULK,ORG_C,NO3_act,K_NO3,redfr,SM,SMmax,Temp, Temp_opt,Th_SM,depth1)

    NO3_t1= NO3_act
    R_Denit= denit
    denit_real=denit_update(R_Denit, NO3_t1, NO3_act)

    dt=1
    nitrates_day2=NO3_budget(NO3_t1,R_Denit,NO3_act,dt)
