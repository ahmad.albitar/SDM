#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#======================================================================================#
#                                   WSDM                                                #
#======================================================================================#
"""WSDM_post: Wetland Soils Denitrification Model tool for extracting time series of points."""
__credits__ = "C. Martinez-Espinosa, S. Sauvage, J. M. Sanchez-Perez, A. Al Bitar"
__authors__ = "A. Al Bitar & C. Martinez-Espinosa"
__copyright__ = "ECOLAB-CESBIO-OMP"
__license__ = "GPL"
__version__ = "0.1"
#======================================================================================#


import numpy as np
import WSDM_plots
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import os
import glob
import WSDM_io
import WSDM_plots
import pandas as pd
from datetime import datetime, date, time, timedelta


def read_poi_file(filepath):
    """
     reads the list of points to be analyzed
      pid=name of each desired node
      lat= list of lat for each desired node
      lon= list of lon for each desired node
    

    Parameters
    ----------
    filepath : string
        path to the file.

    Returns
    -------
    pid : string
       name of the point to be extracted.
    lat : float
        lat of the point to be extracted.
    lon : float
        lon of the point to be extracted.

    """
    
    file = open(filepath, mode = 'r', encoding = 'utf-8-sig')
    headr=file.readline().split('\n')
    headr = headr[0].split(',')
    
    pid_index=headr.index('pid')
    lat_index=headr.index('lat')
    lon_index=headr.index('lon')
    lines = file.readlines()
    file.close()
    pid = list()
    lat = list()
    lon = list()
    for line in lines:
        line = line.split(',')
        line = [i.strip() for i in line]
        pid.append(line[pid_index])
        lat.append(line[lat_index])
        lon.append(line[lon_index])
    return pid,lat,lon



def ts_deni_point(file_dir, points):
    """
   extration of the timeseries over 
    file_dir : 
    points:   

    Parameters
    ----------
    file_dir : string
        simulation directory
    points : float
        coordinates where output model denit and nitrificaiton will be extracted.

    Returns
    -------
    output : TYPE
        DESCRIPTION.

    """
    
    
    pid_name,pid_lat,pid_lon= read_poi_file(points) 
    file_list=sorted(glob.glob(data_dir+os.sep+'/*.npz'))
  
   
    output = [0,0,0,0]
    idx_lon = []
    idx_lat = []
    data0 = np.load(file_list[0])

    for point in range(0,len(pid_name)):
        idx_lon.append(WSDM_io.find_nearest_idx(data0['lon'],float(pid_lon[point])))
        idx_lat.append(WSDM_io.find_nearest_idx(data0['lat'],float(pid_lat[point])))
    
    for infile in range(0, len(file_list)):
        data = np.load(file_list[infile])
        
        for point in range(0,len(pid_name)):
            denit = data['R_NO3'][idx_lat[point],idx_lon[point]]
            nitri = data['NO3_act'][idx_lat[point],idx_lon[point]]
            Date = file_list[infile][-19:-11]
            new_line = (Date, denit, nitri, pid_name[point])
            output = np.vstack([output, new_line])
        print("Done with file", new_line)
                    
    output = pd.DataFrame(output, columns = ['Date', 'Denitrification', 'Nitrification' ,'Point'])
    output = output.drop(index = 0)
    output = output.reset_index(drop = True)
            
    return output


def ts_forc_point(file_dir, points):
    """extration of the timeseries over 
    file_dir : forcing directory (sim dir will contain /out/ dir)
    points:  coordinates where forcing will be extracted. 
    Parameters
    ----------
    file_dir : string
        path to file directory.
    points : int
       id of the point.

    Returns
    -------
    outputforc : TYPE
        DESCRIPTION.

    """

    
    pid_name,pid_lat,pid_lon= read_poi_file(points) 
    file_list=sorted(glob.glob(forc_dir+os.sep+'/*.npz'))
    # file_list= file_list[0:5]
   
    outputforc = [0,0,0,0]
    idx_lon = []
    idx_lat = []
    data0 = np.load(file_list[0])

    for point in range(0,len(pid_name)):
        idx_lon.append(WSDM_io.find_nearest_idx(data0['lon'],float(pid_lon[point])))
        idx_lat.append(WSDM_io.find_nearest_idx(data0['lat'],float(pid_lat[point])))
    
    for infile in range(0, len(file_list)):
        data = np.load(file_list[infile])
        
        for point in range(0,len(pid_name)):
            Tsoil= data['Tsoil'][idx_lat[point],idx_lon[point]]
            SM = data['SM'][idx_lat[point],idx_lon[point]]
            Date = file_list[infile][-19:-11]
            new_line = (Date, Tsoil, SM, pid_name[point])
            outputforc = np.vstack([outputforc, new_line])
        print("Done with file", new_line)
        
            
    outputforc = pd.DataFrame(outputforc, columns = ['Date', 'Soil Temp', 'Soil Moisture', 'Point'])
    outputforc = outputforc.drop(index = 0)
    outputforc = outputforc.reset_index(drop = True)
            
    return outputforc

if __name__== "__main__":
    
    filepath=r"..\inp\validation_points_code.txt"
    data_dir=r"..\out\deni"
    forc_dir=r"..\out\forc"
    deni_list=['NO3_act','R_NO3', 'NO3_t2']
    forc_list=['Tsoil','SM']
    
    pid_name,pid_lat,pid_lon= read_poi_file(filepath) 
    ts_deni_dict=extract_timeseries_deni(sim_dir,deni_list,pid_name,pid_lat,pid_lon)
    ts_forc_dict=extract_timeseries_forc(sim_dir,forc_list,pid_name,pid_lat,pid_lon)
    extract_timeseries(sim_dir,var_list,pid_list,lat_list,lon_list)

    file_dir = r"../WSDM/out"
    points =r"..\sample_run\sample_points.txt"

    output = ts_deni_point(file_dir, points)
    output.to_csv(r"..\sample_run\denit_points.csv")
            
    outputforc = ts_forc_point(file_dir, points)
    outputforc.to_csv(r"..\sample_run\forc_points.csv")
    
###########################Graphe seulement Nitrification et Denit   
    
    path = r"forc_points.csv"

    output = pd.read_table(path, sep = ",")
    DATES = []
    bulk= 1.4
    depth=0.3
    pm_N2O=44
    pm_N=14
    N2O_trans= output.Denitrification*bulk*depth*pm_N2O/(2*pm_N)
    output= output.join(N2O_trans, how="left" )

##### To plot nitrification and Denit
    for point in range(0, len(pid_name)):
        simu =  output[output['Point'] == str(pid_name[point])]
        simu = simu.reset_index(drop = True)
        data_string = np.array(simu['Date'], dtype = 'str')
    
        DATES = []
    
    for l in range(0, len(data_string)):    
        year = int(data_string[l][0:4])
        month = int(data_string[l][4:6])
        day = int(data_string[l][6:8])    
        DATES.append((datetime(year,month,day)).date())
        
    simu['Time'] = DATES
    simu = simu.drop(columns = ['Index: 0', 'Date'])
    fig, ax = plt.subplots(figsize=(27,10), sharex = True)
    
    p0 = plt.plot(simu['Time'], simu['Denitrification'], color = 'orange', linewidth = 1.5, label = "Denitrification")
    p1 = plt.plot(simu['Time'], simu['Nitrification'], color ='blue', linewidth = 1.5, label = "Nitrification")
    
    ax.legend(((p0[0], p1[0])), ('Denitrification', 'Nitrification'), 
              fontsize = 'x-large', bbox_to_anchor = (0.23,1))
    # ax.legend(((denit[0])), 'Denitrification', fontsize = 'x-large', bbox_to_anchor = (0.23,1))
    ax.set_xlabel('Date', fontsize = 'xx-large')
    ax.set_title('Denitrification and Nitrification at' + " "+ pid_name[point])
    ax.set_ylabel('Nitrification/Denitrification en kgN/ha')
    path = r"..\sample_run\out"
    plt.savefig(str(path)+str(pid_name[point])+ '.png', bbox_inches= 'tight')
    
    plt.show()
        
    simu['Time'].dtype

    date_string = np.array(output['Date'], dtype = 'str')
    date_string.dtype
    date_string[0][0:4]

    output.head()
    simu = output
    simu = output[output['Point'] == str(pid_name[0])]
    simu['Point']


#####To plot all the variables

    for point in range(0, len(pid_name)):
        simu =  output[output['Point'] == str(pid_name[point])]
        simu = simu.reset_index(drop = True)
        data_string = np.array(simu['Date'], dtype = 'str')
    
        DATES = []
    
    for l in range(0, len(data_string)):
        
        year = int(data_string[l][0:4])
        month = int(data_string[l][4:6])
        day = int(data_string[l][6:8])
        
        DATES.append((datetime(year,month,day)).date())
        
        
    
    simu['Time'] = DATES
    
    simu = simu.drop(columns = ['Unnamed: 0', 'Date'])
    
    fig, ax = plt.subplots(figsize=(27,10), sharex = True)
    
    p0 = plt.plot(simu['Time'], simu['Denitrification'], color = 'orange', linewidth = 1.5, label = "Denitrification")
    p1 = plt.plot(simu['Time'], simu['Nitrification'], color ='blue', linewidth = 1.5, label = "Nitrification")
    p2 = plt.plot(simu['Time'], simu['Budget'], color ='blue', linewidth = 1.5, label = "Nitrification")
    ax.legend(((p0[0], p1[0])), ('Denitrification', 'Nitrification'), 
              fontsize = 'x-large', bbox_to_anchor = (0.23,1))
    # ax.legend(((denit[0])), 'Denitrification', fontsize = 'x-large', bbox_to_anchor = (0.23,1))
    ax.set_xlabel('Date', fontsize = 'xx-large')
    ax.set_title('Denitrification and Nitrification at' + " "+ pid_name[point])
    ax.set_ylabel('Nitrification/Denitrification en kgN/ha')
    path = r"..\sample_run\out"
    plt.savefig(str(path)+str(pid_name[point])+ '.png', bbox_inches= 'tight')
    
    plt.show()
    

