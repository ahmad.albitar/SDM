# -*- coding: utf-8 -*-

======================================================================================#
#                                   WSDM                                                #
#======================================================================================#
"""WSDM_analysis :Wetland Soils Denitrification Model output analysis."""
__credits__ = "C. Martinez-Espinosa, S. Sauvage, J. M. Sanchez-Perez, A. Al Bitar, R. Cakir"
__authors__ = "A. Al Bitar & C. Martinez-Espinosa"
__copyright__ = "ECOLAB-CESBIO-OMP"
__license__ = "GPL"
__version__ = "0.1"
#======================================================================================#
#import libraries
#####
#  TO UPDATE
import WSDM_io
import WSDM_plots 
import WSDM_tools

import os
import glob
import time
import numpy as np
import scipy.interpolate
import shutil
import rasterio
import matplotlib.pyplot as plt
from datetime import datetime, date, time, timedelta
import pandas as pd

####################
runs_path=r"../Analysis"
param_path=os.path.join(runs_path,"inp","param_analysis.xml")
ease_lat_lon_path=os.path.join(runs_path,"inp","inp_EASE25v2_lon_lat.nc")


###### 0.1 read params file ###################################################
message=("0.2 read params file ")

params = WSDM_io.readXML(param_path)

data_dir= params['data_dir'][0]
data_anc= params['data_anc'][0]
forc_dir= params['forc_dir'][0]
print('Start /end of processing:',params['start_date'][0],'-->',params['end_date'][0])
outpath= os.path.join(runs_path,'out')
shutil.rmtree(outpath, ignore_errors=True)
try:
    os.makedirs(outpath)
except:
    print('[warning] unable to make outdirs: check if they already exist')



###### 0.2 load data ###################################################
   
deni_all_list=sorted(glob.glob(data_dir+os.sep+'/*.npz'))
forc_all_list=sorted(glob.glob(forc_dir+os.sep+'/*.npz'))
data=np.load(deni_all_list[0], mmap_mode='r')
print(data.files)
forc=np.load(forc_all_list[0], mmap_mode='r')
print(forc.files)
Tsoil=forc["Tsoil"]
SM=forc["SM"]
dnit=data["R_NO3"]
deni_files_list=WSDM_io.find_time_deni(deni_all_list,params['start_date'][0],params['end_date'][0])
print ('0.2 load data R_NO3')


###### 0.3  data initializaton ###################################################
init=np.load(deni_all_list[0], mmap_mode='r')
Deni_max=np.zeros(np.shape(init["R_NO3"])) 
deni_sum=np.zeros(np.shape(init["R_NO3"])) 
sum_month=np.zeros(np.shape(init["R_NO3"])) 
Deni_mean=np.zeros(np.shape(init["R_NO3"])) 
print ('0.3 initialization')


##### 0.4  input parameters for convertion ########################################   
anc=glob.glob(data_anc)
anc=np.load(anc[0])
Bulk_int=anc["Bulk"]
depth=params['depth1'][0]

########################################################################### maximum by day by pixel
for i_file in range(len(deni_files_list)):
    print(deni_files_list[i_file][-19:-11])
    data=np.load(deni_files_list[i_file], mmap_mode='r')
    dataD=np.nan_to_num(data["R_NO3"])
    Deni_max= np.maximum(Deni_max,dataD)
    

# #out percentile 90 and 10%
Deni_max99 = np.where(Deni_max>np.percentile(Deni_max[Deni_max!=0], 99),0.001,Deni_max)
WSDM_plots.make_map(data['lat'],data['lon'],Deni_max99,'deni_max','deni_max',0.0,np.percentile(Deni_max[Deni_max!=0], 99))
    

#transforme to kg/ha 
RDenit_kg= Deni_max* Bulk_int* depth*10


WSDM_plots.make_map(data['lat'],data['lon'],Rdenit_mask,'deni_max','deni_max',0.001, np.nan_to_num(Rdenit_mask).max())

np.save(data_dir+'max',RDenit_kg,allow_pickle=True)

RDenit_kg=np.load(r'..\out\denimax.npy')


max_denit = np.nan_to_num(RDenit_kg)
qa=np.quantile(max_denit,0.999)
max_denit = max_denit.max().max()

WSDM_plots.make_map(data['lat'],data['lon'],max_denit,'deni_max','Interannual maximal denitrification (kgN.ha$^{-1}$.yr$^{-1}$)',0.001,qa)

#################### interannual mean in kg/ha
i_file=1
for i_file in range (len(deni_all_list)):
    data=np.load(deni_all_list[i_file], mmap_mode='r')
    deni_sum=np.nan_to_num(data["R_NO3"])+deni_sum
   
#### Total denitrification in mg_g in the whole period 
RDenit_mg_g=np.nan_to_num(deni_sum)
###### Total denitrificaiton in kg_ha in the whole period 
Denit_kg_ha_tot=(RDenit_mg_g*Bulk_int* depth*10)
###### Average denitrification in kg_ha_yr 
R_Denit_kg_yr=Denit_kg_ha_tot/8 #years of simulation period

out_filename='denit_interannual'
np.savez(os.path.join(runs_path,out_filename),R_denit_year=np.float16(R_Denit_kg_yr), Denit_tot=np.float16(Denit_kg_ha_tot))

qa_sum=np.quantile(R_Denit_kg_yr,0.999)
WSDM_plots.make_map(anc['lat'],anc['lon'],R_Denit_kg_yr,'interannual_denit'+params['ROI_name'][0],'Interannual denitrification (kgN.ha$^{-1}$.yr$^{-1}$) for 2011-2019',0.000,qa_sum)


qa_sum=np.quantile(selection_deni,0.999)
WSDM_plots.make_map(data['lat'],data['lon'],selection_deni,'deni_interannual_mean','Interannual mean denitrification 2011-2019 (kgN.ha$^{-1}$.yr$^{-1}$)',0.000,np.quantile(np.nan_to_num(selection_deni,0.999)))

qa_sum=np.quantile(R_Denit_kg_yr,0.999)
WSDM_plots.make_map(data['lat'],data['lon'],R_Denit_kg_yr,'deni_interannual_mean','Interannual mean denitrification 2011-2019 (kgN.ha$^{-1}$.yr$^{-1}$)',0.001,qa_sum)

####### total denitrification by area (kg/ha) 
R_Denit_total=RDenit_kg_yr_tot.sum().sum() #in the total period
R_denit_year=R_Denit_kg_yr.sum().sum() #Interannaul mean


#####plot mean annual denitrification by kg/ha
qa_sum=np.quantile(R_Denit_kg_yr,0.999)
qa_av=np.quantile(Denit_kg_av,0.999)
   
WSDM_plots.make_map(data['lat'],data['lon'],R_Denit_kg_yr,'deni_interannual_mean','Interannual mean denitrification (kgN ha-1 year-1 )',0.001,qa_sum)
WSDM_plots.make_map(data['lat'],data['lon'],Denit_kg_av,'deni_interannual_daily_average','Interannual daily denitrification (kgN-NO3 ha-1 day-1)',0.0001,qa_av)

#################### year sum and annual mean in kg/ha  
 
year=0   
list_year=('2012','2013','2014','2015','2016','2017','2018', '2019')

i_file=1
for year in range(len(list_year)):
    print(list_year[year])
    count_day=0
    for i_file in range (len(deni_all_list)):
        if deni_all_list[i_file][-19:-15]==list_year[year]:
            count_day=count_day+1
            data=np.load(deni_all_list[i_file], mmap_mode='r')
            deni_sum=np.nan_to_num(data["R_NO3"])+deni_sum
            
#transforme to kg/ha
            
    RDenit_kg= deni_sum * Bulk_int*depth*10
    np.savez(os.path.join(outpath,list_year[year]+'_analysis'), denit_annual_sum=np.float16(RDenit_kg))
    deni_sum=np.zeros(np.shape(init["R_NO3"]))

#####################plot mean annual denit 
path = params['data_analysis'][0]

for infile in glob.glob(path + "out_year\\20" +"*.npz"):
    annuel = np.load(infile)
    denit_sum_yr=annuel['denit_annual_sum']
   
    denit_sum = np.nan_to_num(denit_sum_yr)
    print(denit_sum.max())
    qa_sum=np.quantile(denit_sum,0.999)
    qa_av = np.quantile(denit_av, 0.999)
    print(qa_sum)
    title_sum = 'annual denitrification ' + str(infile[107:111] + '(kgN.ha$^{-1}$.yr$^{-1}$)')
    title_av = 'daily denitrification average ' + str(infile[97:101]+ ' (kgN.ha$^{-1}$.yr$^{-1}$) ')
    
    filename=params['ROI_name'][0]+ '_annual_denitrification_' + str(infile[107:111])
    WSDM_plots.make_map(data['lat'],data['lon'],denit_sum_yr,filename,title_sum,0.0001,qa_sum)
    WSDM_plots.make_map(data['lat'],data['lon'],denit_av,title_av,title_av,0.0001,qa_av)


####################### sum  of the same pixel in period of one month and the max is the highest value of the sum.  
month=0
list_month=['01','02', '03', '04','05','06','07','08','09','10','11','12']
list_month_name=['Jan','Feb', 'Mar', 'Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
Deni_max=np.zeros(np.shape(init["R_NO3"])) 

i_file=1
count_day=0
for month in range(len(list_month)):
    print("month=",month+1)
    for i_file in range(len(deni_all_list)):
        # print(deni_files_list[i_file][-19:-11])
        if deni_all_list[i_file][-15:-13]==list_month[month]:
            count_day=count_day+1
            # print(month," and ",i_file)
            data=np.load(deni_all_list[i_file], mmap_mode='r')
            sum_month=np.nan_to_num(data["R_NO3"])+sum_month
            Deni_max= np.maximum(Deni_max,np.nan_to_num(data["R_NO3"]))
    print(sum_month.max())
    print("Day in month",list_month_name[month]," = " ,count_day)
    
    #transforme to kg/ha 
    sum_month_kg= sum_month* Bulk_int* depth*10
    Deni_max_kg= Deni_max* Bulk_int* depth*10
    
    np.savez(os.path.join(outpath,list_month_name[month]+'_analysis'), Denit_tot=np.float16(sum_month_kg), Denit_sum=np.float16(sum_month_kg/len(list_year)), Denit_mean=np.float16(sum_month_kg/count_day), Denit_max=np.float16(Deni_max_kg))
    
    sum_month=np.zeros(np.shape(init["R_NO3"]))
    Deni_max=np.zeros(np.shape(init["R_NO3"])) 
    count_day=0
    
#end of loop   
    
##############################plots for month #####


path_1 = r"..\Analysis\out_month"


for infile in glob.glob(path_1  +"/*_analysis.npz"):
    monthly = np.load(infile)
    denit_sum=monthly['Denit_sum']

    qa_sum=np.nan_to_num(np.quantile(denit_sum ,0.999))
    print(qa_sum)
    print (str(infile[108:111]))
    title_sum = 'monthly denitrification ' + str(infile[108:111] + ' (kgN.ha$^{-1}$.month$^{-1}$) ')
    filename=params['ROI_name'][0] +'_monthly_denitrification_' + str(infile[108:111])
 

    ge_denit_plots.make_map(data['lat'],data['lon'],denit_sum,filename,title_sum,0.0001,200)
  

path = r"..\Analysis\out_month"

for infile in glob.glob(path +"*.npz"):
    
    month = np.load(infile)
    month.files
    
    
    Denit_tot=month['Denit_tot']
    Denit_sum= month ['Denit_sum']
    Denit_mean = month['Denit_mean']
    Denit_max  = month['Denit_max']
    
    denit_sum = np.nan_to_num( Denit_sum)
    denit_av = np.nan_to_num(Denit_mean)
    denit_max = np.nan_to_num(Denit_max)
    
    qa_sum=np.quantile(denit_sum ,0.9999)
    qa_av = np.quantile(denit_av, 0.9999)
    qa_max = np.quantile(denit_max, 0.9999)
    
    title_sum = 'denit_sum_' + str(infile[82:85])
    title_av = 'denit_av_' + str(infile[82:85])
    title_max = 'denit_max_' + str(infile[82:85])
    
    
    ge_denit_plots.make_map(data['lat'],data['lon'],Denit_sum,title_sum,'deni_sum_kg_ha',0.0,qa_sum)
    ge_denit_plots.make_map(data['lat'],data['lon'],denit_av,title_av,'deni_av_kg_ha',0.0,qa_av)
    ge_denit_plots.make_map(data['lat'],data['lon'],denit_max,title_max,'deni_max_kg_ha',0.0,qa_max)



ge_denit_plots.make_map(data['lat'],data['lon'],RDenit_kg,'deni_max','deni_max',0.0,np.nan_to_num(np.percentile(RDenit_kg[RDenit_kg!=0], 99)))
ge_denit_plots.make_map(data['lat'],data['lon'],RDenit_kg,'deni_max','deni_max',0.0,np.percentile(np.nan_to_num(RDenit_kg[RDenit_kg!=0]),99))
ge_denit_plots.make_map(data['lat'],data['lon'],RDenit_kg,'deni_max','deni_max',0.0,np.nan_to_num(RDenit_kg).max())


np.nan_to_num(RDenit_kg).mean()*365



######################################################## 
######### ROI ANNUAL OR MONTHLY CUMULE
###########################################
####update path depending if monthly ot yearly

yearly_path= runs_path +'\out_year'

## monthly_path= runs_path + '\out_month'

ROI=[]

for infile in  glob.glob(yearly_path +"/*.npz"):
    
    data=np.load(infile)    
    year = str(infile[101:105]) 
    denit= (data['denit_annual_sum'])
    denit_sum=np.nan_to_num(denit.sum().sum())    
    ROI.append([year, denit_sum])
    print (year)
    print(denit_sum)
    
ROI = pd.DataFrame(ROI, columns = ['Date', 'Denitrification'])

ROI.to_csv(runs_path+'/annual_denit_'+params['ROI_name'][0]+'.csv')


######################################################## 
######### ROI DAILY
###########################################

ROI=[]

for infile in  deni_all_list:
    
    data=np.load(infile)
    
    fecha = int(infile[-19:-11])
    
    denit= np.nan_to_num(data['R_NO3'])
    # denit= (data['R_NO3'])
    nitri= np.nan_to_num(data['NO3_act'])
    # nitri= (data['NO3_act'])
    # budget= np.nan_to_num(data['NO3_t2'])
    
    
    ########## transformation to kg in ha
    denit_sum=(data['R_NO3']*Bulk_int*depth *10)
    denit_sum=np.nan_to_num(denit_sum.sum().sum())
 
    bulk=np.nan_to_num(Bulk_int)
    denit=np.nan_to_num(denit*bulk)
    mxim=denit.max().max()
 
    nitri=np.nan_to_num(nitri*bulk) 
    denit=np.nan_to_num(denit.sum().sum())
    nitri=np.nan_to_num(nitri.sum().sum())

    denit= denit * depth *10 
    nitri= nitri * depth * 10 
  
    ROI.append([date, denit, nitri,mxim, denit_sum])
    print (date)
    
ROI = pd.DataFrame(ROI, columns = ['Date', 'Denitrification', 'Nitrification', 'maximum', 'denit_sum'])

ROI.to_csv(runs_path+'/daily_denit_'+params['ROI_name'][0]+'.csv')

 
DATES = []

for l in range(0, len(ROI)):
   

    date_file = np.array(ROI['Date'], dtype = 'str')
    year = int(date_file[l][0:4])
    month = int(date_file[l][4:6])
    day = int(date_file[l][6:8])
        
    DATES.append((datetime(year,month,day)).date())

ROI['Time'] = DATES

fig, ax = plt.subplots(figsize=(27,10), sharex = True)
    
p0 = plt.plot(ROI['Time'], ROI['Denitrification'], color = 'black', linewidth = 1.5, label = "Denitrification")
    
ax.legend((p0[0]), 'Denitrification in kgN', fontsize = 'x-large', bbox_to_anchor = (0.23,1))
ax.set_xlabel('Date', fontsize = 'xx-large')
ax.set_title("Total Denitrification (kgN) over South America between 2012 - 2019")
ax.set_ylabel('Denitrification kgN')

plt.savefig(runs_path + '/daily_denit'+ params['ROI_name']+ '.png', bbox_inches= 'tight')
    
plt.show()
